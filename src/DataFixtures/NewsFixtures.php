<?php

namespace App\DataFixtures;

use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\News;
use Faker\Factory;
use App\Entity\User;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $slug = new Slugify();
        $user = $manager->getRepository(User::class)->findAll();

        for ($i=1; $i<=50; $i++){
            $news= new News();
            $news->setName($faker->word(3,true));
            $news->setCreatedAt(new \DateTimeImmutable());
            $news->setUpdatedAt(new \DateTimeImmutable());
            $news->setContent($faker->paragraph(5));
            $news->setIsPublished($faker->boolean(90));
            $news->setImage('0'.$i.'.png');
            $news->setSlug($slug->slugify($news->getName()));
            $news->setAuthor($user[$faker->numberBetween(0, count($user) -1)]);

            $manager->persist($news);

        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}