<?php

namespace App\DataFixtures;

use App\Entity\User;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private array $genders = ['Male', 'Female'];
    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher) {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $slug = new Slugify();
        for($i = 1; $i <= 50; $i++) {
            $user = new User();
            $gender = $faker->randomElement($this->genders);
            $user->setFirstName($faker->firstName($gender));
            $user->setLastName($faker->lastName);
            $user->setEmail($slug->slugify($user->getFirstName()).$slug->slugify($user->getLastName()).'@gmail.com');
            $gender = $gender == 'Male' ? 'm' : 'f';
            $user->setImage('0'.($i + 10).$gender.'.jpg');
            $user->setPassword($this->hasher->hashPassword($user, 'password'));
            $user->setCreatedAt(new \DateTimeImmutable());
            $user->setUpdatedAt(new \DateTimeImmutable());
            $user->setLastLogAt(new \DateTimeImmutable());
            $user->setIsDisabled($faker->boolean(10));
            $user->setRoles(['ROLE_USER']);
            $manager->persist($user);
        }
        $manager->flush();

        // John Doe role admin
        $user = new User();
        $user->setFirstName('John');
        $user->setLastName('Doe');
        $user->setEmail('john.doe@gmail.com');
        $user->setImage('075m.jpg');
        $user->setPassword($this->hasher->hashPassword($user, 'password'));
        $user->setCreatedAt(new \DateTimeImmutable());
        $user->setUpdatedAt(new \DateTimeImmutable());
        $user->setLastLogAt(new \DateTimeImmutable());
        $user->setIsDisabled(false);
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        $manager->flush();

        // Pat Mar role super-admin
        $user = new User();
        $user->setFirstName('Pat');
        $user->setLastName('Mar');
        $user->setEmail('pat.mar@gmail.com');
        $user->setImage('074m.jpg');
        $user->setPassword($this->hasher->hashPassword($user, 'password'));
        $user->setCreatedAt(new \DateTimeImmutable());
        $user->setUpdatedAt(new \DateTimeImmutable());
        $user->setLastLogAt(new \DateTimeImmutable());
        $user->setIsDisabled(false);
        $user->setRoles(['ROLE_SUPER_ADMIN']);
        $manager->persist($user);

        $manager->flush();
    }
}